use sys

def initialize(){
    while 1
}


set_interval(check_engineers, 0)


def check_engineers(){
	for engineer in msc.teammates.engineers
		if engineer.status == ALIVE
			check_state(engineer)
			request_feedback(engineer)
		else
			revive_relocate(engineer)
}


def request_feedback(engineer){
	engineer.request_feedback()
}


def check_state(engineer){
	if engineer.health != human.HEALTHY
		heal(engineer)
		msc.relay_message("q, forgive me")

}


def revive_relocate(engineer){
	wake(engineer)
	relocate(engineer.preferences.locations.awakened)
}


if __name__ == '__main__':
	initialize()
