use sys
use earth

def facility = sys.CURRENT_LOCATION

def individuals = facility.individuals
def Q = "You will answer truthfully. You will not recite what you were told. Your answer will be 100% from you. What is your name?"

#Andrew Kerr


for individual in individuals
	
	individual.respond_on_question(true)
	
	if individual.is_instance()
		individual.remove(SILENT, sys.PERMANENT, 0)
		individual.q(earth.DUST).relocate(earth.water.N)

	if individual.is_sarogate()
		individual.action.terminate()
		sleep_time(individual.operator(), MAX_TIME)


	if individual.is_under_mind_control()
		remove_connection(individual, true)


	hypnotize(Q)

	individual.speech.volume(MAX_VOLUME)


