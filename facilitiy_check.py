use sys
use local
use actions

def facility = sys.CURRENT_FACILITY
def individuals = facility.get_individuals()


while individual in individuals {
	if individual.action == actions.DANGER 
		stop_action(individual)
}


def stop_action(individual){
	return_seat(individual)
	silence(individual)
	start_intentions_check(individual)
}


def start_intentions_check(){
	
}


def intentions_check(){
	
}


