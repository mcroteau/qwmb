use sys


def attention_seeker1 = locate_internal_memory("Terry Helmeuller", "Anchorage, AK")
def attention_seeker2 = locate_internal_memory("Terry Ameline", "Anchorage, AK")
def attention_seeker3 = locate_internal_memory("Tony Ameline", "Anchorage, AK")

attention_seeker1.heal()
attention_seeker2.heal()
attention_seeker3.heal()
