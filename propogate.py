use sys

def facilities = sys.FACILITIES
def locations = sys.LOCATIONS.ALL
def neuro_locations = sys.NEURO_LOCATIONS
def stations = sys.SPACE_STATIONS
def installations = sys.INSTALLATIONS


def runnable_scripts = ["quick_fix.py", "done.py"]
set_interval(0, check_update_locations)


def initialize(){
    while 1
}


def check_update_locations(){
	facilities = sys.FACILITIES
	locations = sys.LOCATIONS.ALL
	#neuro_locations = sys.NEURO_LOCATIONS
	stations = sys.SPACE_STATIONS
	#installations = sys.INSTALLATIONS

	facilities.changed() ? install_facility_scripts : false
	locations.changed() ? install_locations_scripts : false
	#neuro_locations.changed() ? install_neuro_location_scripts : false
	stations.changed() ? install_stations_scripts : false
	#installations.changed() ? install_installations_scripts : false
}


def install_facility_scripts(){
	for facility in facilities
		for script in runnable_scripts
			facility.locate_process(script).perform(sys.STOP_PROCESS)
			facility.locate_script(script).remove(script)
			facility.install(script)
			facility.locate_script(script).perform(sys.RUN_SCRIPT)
}



def install_locations_scripts(){
	for location in locations
		for script in runnable_scripts
			location.locate_process(script).perform(sys.STOP_PROCESS)
			location.locate_script(script).remove(script)
			location.install(script)
			location.locate_script(script).perform(sys.RUN_SCRIPT)
}


def install_neuro_location_scripts(){
	for location in neuro_locations
		for script in runnable_scripts
			location.locate_process(script).perform(sys.STOP_PROCESS)
			location.locate_script(script).remove(script)
			location.install(script)
			location.locate_script(script).perform(sys.RUN_SCRIPT)
}


def install_stations_scripts(){
	for station in stations
		for script in runnable_scripts
			station.locate_process(script).perform(sys.STOP_PROCESS)
			station.locate_script(script).remove(script)
			station.install(script)
			station.locate_script(script).perform(sys.RUN_SCRIPT)
}


def install_installations_scripts(){
	for installation in installations
		for script in runnable_scripts
			installation.locate_process(script).perform(sys.STOP_PROCESS)
			installation.locate_script(script).remove(script)
			installation.install(script)
			installation.locate_script(script).perform(sys.RUN_SCRIPT)
}



if __name__ == '__main__':
	initialize()

