use sys
use msc
use earth
use human
use actions



def count
def network

def MAX_COUNT = 1

def APOLOGIES = "Forgive me, I'm unable to help you."
def SIMPLE_REFLECTION = "Do I really want to do this?"



set_interval(0, respect_people_at_all_times)



def respect_people_at_all_times(){
	initialize_variables()

	for person in network
		if ((person.action not equal msc.POSITIVE_ACTION and violating_anyone_in_network(person)) and
				(person.action.intention <= msc.FANTASTIC_INTENTION and 
					person.action.self_serving != msc.self_serving_test ||
						person.action.is_perverted(msc.standards.perversion)))
			count++
			if count == MAX_COUNT
				person.action.source.relay_message(APOLOGIES).terminate()
				person.action.reconsideration(actions.NEVER_RECONSIDER)
				person.consider_disassocatiation(person.action.source)
			else
				count = 0
				sys.relay_message(SIMPLE_REFLECTION)
}



def violating_anyone_in_network(person){
	def violates = false
	for connection in network
		if person.reaction >= msc.reactions.NEGATIVE
			violates true

	return violates
}


def populate_warmth_network(){
	#consider their connections
	for connection in msc.connections

		if connection not in network
		network.add(connection)

			if connection.connections

				for m in connection.connections
					def recursive_connections = get_recursive_connections(m, [])
					network = network + recursive_connections

}


def get_recursive_connections(individual, connections){
	for individual in individual.connections
		if individual.connections
			for c in individual.connections
				if c not in connections
				connections.add(c)
				get_connetions(connections)

	return connections
}


def initialize_variables(){
	count = 0
	network = []
	populate_warmth_network()
}


#def check_health(){
#	if sys.HEALTH = msc.health.UNHEALTHY
#		exit
#}
