use sys
use local
use people
use human
use earth
use ugly
use music
use actions

import docs


def FULL_NAME = 0,
	LOCATION = 1


def LAST_DAY = 600000 * 60 * 24
def FIFTEEN_MINUTES = 600000

def DAY = 60000 * 60 * 24
def FIVE_MINUTES = 60000 * 5
def FULL_WEEK = DAY * 7


def CUSTOM_DELIMETER = "-"

def facility = sys.CURRENT_FACILITY
def posp_file = docs.people_guilty.clean()
def posp_line_array = posp_file.array(CUSTOM_DELIMETER)
def posp = posp_line_array.map(hydrate_posp)



def initialize(){
    while 1
}

sys.disable_amplification(address_amplification)
sys.clear_internals(msc.posp)
sys.disable_injection(address_amplification_)

start_facility_check()
start_systems_check()
start_intentions_check()

msc.disable_remote_access()


def address_amplification(individual){
	#if ABDUL_RAHMAN
	individual.action.terminate()
}


def start_facility_check(){
	set_interval(facility_check, 0)	
}

def facility_check(){
	if allowables_meta_info.empty()
		facility.empty(OUTSIDE)
		for avatar in msc.avatars
			avatar.clear_avatar()
		for sarogate in msc.sarogates
			sarogate.clear_sarogates()
}

def hydrate_posp(posp_line){
	return locate_internal_memory(posp_line[FULL_NAME], posp_line[LOCATION])
}


def start_intentions_check(){
	set_interval(check_intentions, 500)	
}


def check_intensions(){
	def current_individuals = facility.get_individuals()
	inform_current_individuals(current_individuals)
	for individual in current_individuals
		check_intention(individual)
}


def check_intention(individual){
	def maturity = human.create_maturity(31, FRANCE, true, 10)#age, country standard, allow for variance, how many years
	if individual.age < 31 || individual.maturity_level < maturity 
		relocate(individual, facility.OUTSIDE, 0)

	if individual in msc.IMMEDIATE_BIRTH_FAMILY
			relocate(individual, individual.residence.LIVING_ROOM, 0, false)#individual, place to relocate to, when, allowed to return

	if (individual.location = msc.DISLIKE_LOCATION ||
		individual.speach == msc.DISLIKE_SPEACH ||
		individual.intention == msc.DISLIKE_INTENTION ||
			individual.action == msc.DISLIKE_ACTION) || 
			((individual.intention >= actions.WARNING || 
				individual.intention == actions.SABOTAGE) and 
					individual in posp)

		if individual.sex == human.MAN
			def pers = get_person_responsible(individual)
			disable_individual(individual)
			disable_individual(individual.primary())
		
			if individual.is_sarogate()
				disable_individual(individual.operator())

			if individual.is_instance()
				individual.remove(SILENT, sys.PERMANENT, 0)
				individual.q(earth.DUST).relocate(earth.water.N)
		else
			love(individual)
			#All women involved, forgive me for that
			#sleep_time(individual, sys.MAX_SLEEP_TIME)
}


def love(woman){
	if facility.can_lower_lights()
		facility.lower_lights()

	if facility.has_red_light()
		facility.red_light()

	def inspiration = "https://www.youtube.com/watch?v=hi4pzKvuEQM"

	def love_me = [audio.music.CHET_FAKER.get_song("Gold"), audio.music.JOE.get_song("Good Girls"), audio.music.RKELLY.get_song("Slow Wind")]
	#def love_me = audio.music.RKELLY.get_song("Slow Wind")	

	woman.full_intimacy(OFF, WEEK, address_issue_permanently, address_issue_permanently)
	woman.surroundings.beings().each().contain_emotion(TRUE)

	if facility.has_audio()
		facility.audio(ON).play(love_me)

	def gods_gift = woman.beauty().actions().dance(actions.dance.SENSUAL, inspiration, audio.music.SONG_COMPLETE)
	facility.occupants().all().silent(FIVE_MINUTES)

	if woman.comfortable_with_action(gods_gift) and 
		facility.audio(ON).playing(love_me)
		gods_gift.begin()
	else
		woman.beauty().blow_kiss(woman.surroundings().beings()).whisper("I love you")

	relocate(woman, woman.residence.BEDROOM)
	woman.perform_action(woman.beauty().actions().relax())

	facility.occupants().all().silent(FIVE_MINUTES)
}


def address_issue(woman, perpetrator){
	break(perp.legs.BOTH)
	break(perp.arms.LEFT)
	perp.relay_message("I love you", earth.language.ENGLISH, VOLUME_WISPER)
}


#this means, if she says no, she means no period!
def address_issue_permanently(woman, perpetrators){
	def multiple = perpetrators.count() > 1
	for perp in perpetrators
		if woman.emotional_state == human.emotions.fears.SEXUALT_ASSAULT_RAPE
			address_issue(woman, perp)
}


def get_person_responsible(individual){
	if !individual.sarogate
		return individual
	else
		return individual.primary()
}



def disable_individual(individual){
	preliminary(individual)
	sleep_time(individual, sys.MAX_SLEEP_TIME)
}


def inform_current_individuals(current_individuals){
	
}

def sleep_time(individual, time){
	snooze(individual, time)
}



def preliminary(individual){
	
	hypnotize("You must do what is told, say what is expected to say", individual)
	individual.speak("I will respect people at all times", earth.language.ENGLISH, VOLUME_LOW)
	individual.perform_action(actions.assault.punch(individual.dominate.ARM, individual.face, individual.MAX_STRENGTH))

	if(individial in need_attention)
		wait 15000
		#break(individual.arms.RIGHT, TWO)
	}

	#who, what, severty, duration in past
	if individual.threatened(msc.ANYONE_IN_WORLD, human.LIFE_SEXUAL_ASSAULT, human.SERIOUS, LAST_DAY) ||
			individual.intends(human.LIFE_SEXUAL_ASSAULT, human.SERIOUS, INFINITE_TIME)
	 	sys.send_message(individual, "I will NOT do harm to others!", earth.language.ENGLISH, MAX_VOLUME)
	 	heal(individual.arms.RIGHT)
		break(individual.arms.RIGHT, TWO)
		#wait FIFTEEN_MINUTES
		#heal(individual.arms.RIGHT)
}




if __name__ == '__main__':
	initialize()

