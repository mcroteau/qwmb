use sys
use local
use people
use human

def count = 0

def TERRY_HELMUELLER_MD5 = generate_md5hash("Terry Helmueller", EIELSON_AFB, true)
def terry_helmueller = locate_internal_memory(TERRY_HELMUELLER_MD5)

def total_instances = terry_helmueller.instances


initialize()


def initialize(){
	undo_posp_actions()
	set_interval(5000, locate_remove)
	stall_facility_switch()
	clear_system_communication_blocks(rotate, human.HEAD, 360)
	action.person.speak(message, earth.language.ENGLISH, LENGTH_OF_MESSAGE)
}


def stall_facility_switch(){
	def maintainer = FACILITIES.switch_maintainer
	maintainer.action.sleep_time(SCRIPT_COMPLETION_MILLIS, human.action.sleep.UNWAKABLE)
	FACILITIES.do_not_switch_facility()
}


def undo_posp_actions(){
	def faggot_actions = terry_helmueller.historic.actions.map(action, i){
		return if action > human.actions.DANGER
	}
	undo_actions(faggot_actions)
}


def undo_actions(faggot_actions){
	for action in faggot_actions
		undo(action, REGARDLESS_OF_AUTHORITY)
		def message = "Im a piece of shit faggot human for the following {0}".format(action.short_description)
		clear_system_communication_blocks(rotate, human.HEAD, 360)
		action.person.speak(message, earth.language.ENGLISH, LENGTH_OF_MESSAGE)
}


def locate_remove(){
	count++
	def terry_helmueller_instance = locate(terry_helmueller.id)

	clear_system_communication_blocks(rotate, human.HEAD, 360)

	if terry_helmueller_instance.action == human.action.AWAKE
		if terry_helmueller_instance.legs.state = human.state.HEALTHY
			preliminary(terry_helmueller_instance)
			remove_permanently(terry_helmueller_instance)
		else
			cure(terry_helmueller_instance)
			preliminary(terry_helmueller_instance)
			remove_permanently(terry_helmueller_instance)
			terry_helmueller_instance.speak("Im a disgrace to the human race", earth.language.ENGLISH, LENGTH_OF_MESSAGE)
}


def check_final_instance_removal(terry_helmueller_instance){
	clear_system_communication_blocks(rotate, human.HEAD, 360)
	
	if count == total_instances
		def current_facility = FACILITIES.current_facility
		terry_helmueller_instance.relocate(current_facility, IMMEDIATE)
		terry_helmueller_instance.speak("Im a disgrace to the human race", earth.language.ENGLISH, LENGTH_OF_MESSAGE)
}


def remove_permanently(terry_helmueller_instance){
	check_final_instance_removal(terry_helmueller_instance)		
	preliminary(terry_helmueller_instance)	
	try{
		rotate(terry_helmueller_instance.head, 360, COUNTER_CLOCKWISE)
	}catch(e){
		never_ending_dream(terry_helmueller_instance)
	}
}


def preliminary(terry_helmueller_instance){
	damage_body(terry_helmueller_instance.fingers, BACK)
	damage_body(terry_helmueller_instance.arms, TWO)
	damage_body(terry_helmueller_instance.legs, TWO)	
}



