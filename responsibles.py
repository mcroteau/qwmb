use sys
use actions
use earth

def DAY = 60000 * 60 * 24
def WEEK = DAY * 7

def individual = msc.people_nearby(msc.VISUAL_FOCUS)
def individuals = individual.source.all()

def preferences = [
	"Comps", "Donation : https://www.generosity.com/fundraisers/i-need-financial-assistance--3", ""
]


def Q = "You will be honest from here on out. Tell everyone connected the truth about what you are involved in. DoD requests you leave himm alone"


for individual in individuals

	address_issue(individual, VOLUME_LOW)  #custom method, interval, duration, params
	
	if individual.is_instance()
		address_issue(individual.primary(), VOLUME_LOW)  #custom method, interval, duration, params
		individual.primary().operator().on_watch(true, status_check(check_intentions, 0, WEEK, [individual]))
	
	if individual.is_operator()
		address_issue(individual.operator(), VOLUME_LOW)  #custom method, interval, duration, params
		individual.operator().on_watch(true, status_check(check_intentions, 0, WEEK, [individual]))
	
	individual.on_watch(true, status_check(check_intentions, 0, WEEK, [individual]))



def address_issue(individual, volume, current_action){
	hypnotize(Q, 60000 * 60 * 24
	individual.speak("MOVE! I will respect people at all times!", earth.language.ENGLISH, volume)
	if current_action 
		current.action.terminate()
		def action = human.actions.create_action(msc.preferences.actions.good.get_random_or_request())
		individual.perform_action(action, address_issue)
		individual.clear_on_watch()
		individual.on_watch(true, status_check(check_intentions, 0, WEEK, [individual]))

	#individual.perform_action(actions.assault.punch(individual.dominate.ARM, individual.face, individual.MAX_STRENGTH))
}




def check_intentions(individual){
	def current_action = individual.action
	if (current_action == actions.RETALIATION and current_action.destination == msc) || 
		(current_action == actions.PAYBACK and current_action.inspiration == msc)

		if individual.is_instance()
			address_issue(individual.primary(), VOLUME_WHISPER, current_action)  #custom method, interval, duration, params
		
		if individual.is_operator()
			address_issue(individual.operator(), VOLUME_WHISPER, current_action)  #custom method, interval, duration, params
				
		address_issue(individual, VOLUME_WHISPER, current_action)
}