use sys
use msc

def individuals = undo_action(msc.LAST_SEVERE_ACTION_TAKEN, go_to_sleep)

def go_to_sleep(){
	for individual in individuals
		sleep_time(individual, MAX_SLEEP_TIME)
}

