use sys
use msc
use local

def count
def network

def MAX_COUNT = 3

def APOLOGIES = "Forgive me, Im unable to help you."
def SIMPLE_REFLECTION = "Do I really want to do this?"




set_interval(0, respect_people_at_all_times)



def respect_people_at_all_times(){
	initialize_variables()
	check_health()
	def instance_self = self
	if (instance_self.action not equal msc.POSITIVE_ACTION and violating_anyone_in_network(instance_self))
		count++
		if count == MAX_COUNT
			count = 0
			self.action.source.relay_message(APOLOGIES)
			self.action.terminate().reconsider(actions.NEVER_RECONSIDER).consider_disassocatiation(instance.action.source)
			exit
		else
			sys.relay_message(SIMPLE_REFLECTION)
			exit
}


def violating_anyone_in_network(){
	def violates = false
	for connection in network
		if msc.reaction == msc.reactions.NEGATIVE
			violates true

	return violates
}


def populate_network(){
	#consider their connections
	for connection in msc.connections

		if connection not in network
		network.add(connection)

			if connection.connections

				for m in connection.connections
					def recursive_connections = get_recursive_connections(m, [])
					network = network + recursive_connections

}


def get_recursive_connections(individual, connections){
	for individual in individual.connections
		if individual.connections
			for c in individual.connections
				if c not in connections
				connections.add(c)
				get_connetions(connections)

	return connections
}


def initialize_variables(){
	count = 0
	network = []
	populate_network()
}


def check_health(){
	if sys.HEALTH = msc.health.UNHEALTHY
		exit
}
